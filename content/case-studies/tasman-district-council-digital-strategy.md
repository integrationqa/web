---
weight: 3
slider_filter:
- case-study
work:
- Strategy

title: Creating clarity and alignment on digital strategy through openness, transparency and self assessment.
description: The last thing Tasman District Council (TDC) wanted was an independent viewpoint on digital strategy and an industry assessment on its maturity.  They didn't want to go through an exercise to find out where they were at only to then ask. What do we do?  They wanted to move forward.

featuredImage:
modifierClass: white-logo
clientLogo: tasman.png
tag:
  - value: Digital Strategy
#metricType1: Manual Deploy
#metricValue1: 3 days
#metricType2: Auto Deploy
#metricValue2: 90 mins

section:
  - title: The Context
    description: TDC has a large and changing region, extensive physical assets and a huge service catalogue.  They wanted to advance its thinking and awareness on digital readiness across staff and leadership to best meet the growing and changing needs of their customers.
  - title: The Approach
    description: Dan Randow of Sentify (previously IntegrationQA) carried out a mini-assessment using three simple questions. “What does ‘digital’ mean?”, “What level of digital maturity is needed?” and “What is the current level of digital maturity?”. Dan used the responses to these questions to create an MVP self-assessment. He then worked with TDC to design workshops that involved staff in the assessment at scale.
  - title: The Workshops
    description: Around 70 people attended each of three 90 minute workshops. The workshops were highly participative with lively discussion about the meaning of “digital” at TDC. Participants worked in groups to assess their needs and current levels of digital maturity. They focused their discussions not on the internal structure but on service groups used by customers.
  - title: The Outcome
    description: The report summarised the workshop conversations so it was real, relevant and meaningful. It highlighted how digital complements support staff to provide the analogue channels that many customers prefer. In each of TDC’s six main service groups, it identified where investment in front-facing or back-end services could make the most difference.

# testimonial
testimonial:
  by: Peter Darlington
  designation: Information Services Manager
#  avatar:
  text: The process was just as valuable as the report.  It got people talking about getting with the digital age. That raised people’s awareness and their sense that they are part of the organisation taking a step forward.  With a more conventional approach, we would have got an outside expert opinion. This was a digital age approach that fully involved our organisation.

# Table
#tableTitle:
#  left: What we did
#  right: Incremental Value
#tableContent:
#  - left: 
#    right: 

---
