---
weight: 3
slider_filter:
- case-study
work:
- DevOps
- Cloud

title: Eliminating Infrastructure Drift and Reducing RTO From Days To Minutes
description: Merging infrastructure management with the application build pipeline brings development & operations together.  That sounds like DevOps.
featuredImage:
clientLogo: nzgsfa.png
modifierClass: white-logo
metricType1: Manual Deploy
metricValue1: 3 days
metricType2: Auto Deploy
metricValue2: 90 mins

section:
  - title: The Client
    description: The Government Superannuation Fund Authority is an autonomous Crown Entity, established in October 2001. Its functions are to manage and administer the Government Superannuation Fund and the GSF Schemes in accordance with the Government Superannuation Fund Act of 1956.  Sentify works closely with the organisation to maintain and support the administration solution necessary to effectively manage the funds in AWS and execute the vital work GSF does every day.
  - title: The Problem
    description: After several years of infrastructure drift, lack of documentation, and the ongoing maintenance of its tech, GSF was in an infrastructure gridlock.  They had no confidence within their organisation to make changes to their infrastructure.  The idea of turning off infrastructure that was not being regularly used was unthinkable and there was no ability to take advantage of any new native technologies.  Leadership also feared that a corrupted or failed resource would require several days to rebuild from scratch, which would have a major impact on their  business operations.
  - title: The Outcome
    description: Sentify built up a robust set of version-controlled infrastructure.  Our focus was to create a rich set of unit and deployment tests that could provision and verify the full solution within 90 mins.  These provided the building blocks to begin leveraging native AWS resources with confidence, reduce maintenance effort, and have the flexibility to turn off systems that were not in use, which saved compute costs.

# testimonial
#testimonial:
#  by: Philippa Drury
#  designation: General Manager
#  avatar:
#  text: Sentify has reduced our operational risk and revitalised our confidence in the technology choices we made.  We now have a partner that allows us to make informed choices together.

# Table
#tableTitle:
#  left: What we did
#  right: Incremental Value
#tableContent:
#  - left: Audit of all resources against billing
#    right: Improved understanding and confidence in resources
#  - left: Audit of all resources against billing
#    right: Improved understanding and confidence in resources
---

