---
weight: 3
slider_filter:
- case-study
work:
- Strategy

title:  Increasing digital confidence and capitalising on internal knowledge.
description: UnitingCare recognised they needed structure and guidance on a range of ICT issues.  As a not for profit resources have to take on multiple roles and are often a spread thin. They feared something might slip between the gaps.

featuredImage:
clientLogo: unitingcare.png
modifierClass: white-logo
section:
  - title: The Client
    description: UnitingCare Australia is the national body for the Uniting Church’s community services network.  It also acts as an agency of the Assembly of the Uniting Church in Australia.  The Uniting Church's community service network supports 80,000 staff and volunteers across 1,600 sites.  In all, this organisation supports 1.4m people.
  - title: The Problem
    description: UnitingCare recognised they needed guidance on a range of ICT issues including ICT Strategy, Security, capacity management, data management, monitoring and a sounding board on ICT procurement, M365 usage, staff training and leverage Cloud capabilities.
  - title: CIO as a Service
    description: Gabe Blount is a Sentify senior consultant providing a CIO as a Service function to help organisations shape fit for purpose strategy and provide guidance on day to day operations.
  - title: Working Together
    description: Together we have been able to form a vision and strategy that provides clarity and action to move forward with confidence.  A security review has also provided additional comfort and a technology road provides context against the ever evolving landscape.
  - title: Next Steps
    description: With an action plan in place and a sense of where they are at, UnitingCare move forward knowing they have a partner to turn when needed.

# testimonial
testimonial:
  by: Gabe Blount
  designation: Senior Consultant
#  avatar:
  text: UnitingCare Australia subscribes to our CIO as a Service.  We work with UnitingCare Australia in augmenting their voice for social justice and poverty elimination.  These admirable causes offer us a unique opportunity to support those who support others and we are proud to be part of their network.

# Table
#tableTitle:
#  left: 
#  right: 
#tableContent:
#  - left: 
#    right: 
--- 