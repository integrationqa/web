#$1 being the Environment Variable: $pages_endpoint
#$2 being the Environment Variable: genie_key
echo "initiating script"
lighthouse --chrome-flags="--headless --no-sandbox" $1 --output html --output-path ./report.html
if [ $? -eq 0 ]
then
  curl -i \
    -H "Authorization: GenieKey $2" \
    -X GET "https://api.opsgenie.com/v2/heartbeats/Sentify/ping"
fi
